#!/bin/bash
#
# menu2 and some bug fixes by Rupe 10.07.2015
# The original creator unknown.
# *I* take No responsibility for any damages caused to your system.
# This is alpha ... feel free to make it your own.


export PATH=$PATH:`pwd`

while :; do
    OPTION=$(whiptail --title "Kali Wifi Tools Menu" \
      --menu "Select a script" 15 60 7 \
      "1)" "Change Mac Address" \
      "2)" "Crack Handshake" \
      "3)" "Crack Handshake Without Dictionary" \
      "4)" "Crack WiFi" \
      "5)" "Deauthorize Target Router" \
      "6)" "Generate List For Cracking" \
      "7)" "Mass Create APs With Generated Names" \
      "8)" "Mass Create APs With Preset Names" \
      "9)" "Jam All Nearby WiFi" \
      "10)" "Simple MITM Attack" \
      "11)" "Capture Raw Packets" \
      "12)" "Start|Stop Monitor Mode" \
      "13)" "Scan for LAN devices" \
      "14)" "Check Internet Adapters" \
      "15)" "Shutdown Computer Instantly" \
      "16)" "Change All Mac Addresses" 3>&1 1>&2 2>&3)

    exitstatus=$?

    if [ ${exitstatus} -eq '0' ]; then
      case ${OPTION} in
        "1)") 1 ;;
        "2)") 2 ;;
        "3)") 3 ;;
        "4)") 4 ;;
        "5)") 5 ;;
        "6)") 6 ;;
        "7)") 7 ;;
        "8)") 8 ;;
        "9)") 9 ;;
        "10)") 10 ;;
        "11)") 11 ;;
        "12)") 12 ;;
        "13)") 13 ;;
        "14)") 14 ;;
        "15)") 15 ;;
        "16)") 16 ;;
      esac
    else
      echo "You chose to cancel."
      exit 1
    fi
done
