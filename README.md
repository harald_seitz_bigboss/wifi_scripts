# Installation Instruction: #

*These scripts work on Kali Linux 2.0 only*

**Download the zip here**: http://bit.ly/1MWMt1w

Screenshot: http://bit.ly/1kRshIe

1. unzip ruped24-wifi_scripts.xxx.zip
 
2. sudo chmod -R 755 ruped24-wifi_scripts.xxx
 
3. cd ruped24-wifi_scripts.xxx
 
4. sudo ./menu2.sh

**List of tools:**
 
1. Change Mac Address
2. Crack Handshake
3. Crack Handshake Without Dictionary
4. Crack WiFi
5. Deauthorize Target Router
6. Generate List For Cracking
7. Mass Create APs With Generated Names
8. Mass Create APs With Preset Names
9. Jam All Nearby WiFi
10. Simple MITM Attack
11. Capture Raw Packets
12. Start|Stop Monitor Mode
13. Scan for LAN devices
14. Check Internet Adapters
15. Shutdown Computer Instantly
16. Change All Mac Addresses

Enjoy ;)